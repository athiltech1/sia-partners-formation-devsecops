import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private AuthService:AuthService, private router: Router) { }
  ngOnInit(): void {
  }
  login(e:any){
     e.preventDefault()
     this.AuthService.getAuthUser().subscribe(data=> this.router.navigate(['/home'],{queryParams:{session_id:data.uid,User:data.first_name}})  
    )
  }
}

